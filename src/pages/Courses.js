import { Fragment, useEffect, useState } from 'react';
//import coursesData from "../data/coursesData";

import CourseCard from '../components/CourseCard';
import { Row } from 'react-bootstrap';


/* 
    the course in our CourseCard component can be sent as a prop
        prop - is a shorthand for "property" since the components are considered as objects in ReactJs
    
    the curly braces {} are used for props to signify that we are providing information using JS expressions rather than hard coded values which use double quotes ""
*/
export default function Courses()
{   
    const [courses, setCourses] = useState([]);

    useEffect(() =>
    {
        // since we used .env.local, we have to restart the application because environment variables take effect at the initial start of the app
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setCourses(data.map(course => {
                return(
                    <CourseCard key={course._id} courseProp= {course} />
                )
            }))
        })
    }, [])

    //const courses = coursesData.map(course => <CourseCard key={course.id} courseProp= {course} /> );

    return (
        <Fragment>
            <Row className="p-2">{courses}</Row>
        </Fragment>
    )
}