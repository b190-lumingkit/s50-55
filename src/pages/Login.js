import { Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from "react";
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { UserContext } from '../UserContext';

export default function Login()
{
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState(""),
        [password, setPassword] = useState(""),
        [isActive, setIsActive] = useState(false);

    const retrieveUserData = (token) =>
    {
        fetch('http://localhost:4000/users/details',
        {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data =>
        {
            console.log(data);
            setUser({
                id: data._id, 
                isAdmin: data.isAdmin,
                email: data.email
            })
        })
    }

    useEffect(() => 
    { 
        (email !== "" && password !== "") ? setIsActive(true) : setIsActive(false) 
    }, [email, password]);

    function loginUser(e)
    {
        e.preventDefault();

        // fetch method will communicate with our backend application providing it with a stringified JSON coming from the body
        /* 
            SYNTAX:
                fetch('url', {options})
                .then(res => res.json())
                .then(data => {})

                .then is used to create promise chain to catch the data from the fetch method
        */
        fetch('http://localhost:4000/users/login',
        {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (typeof data.access !== 'undefined')
            {
                localStorage.setItem('token', data.access)
                retrieveUserData(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            }
            else
            {
                Swal.fire({
                    title: "Auth failed",
                    icon: "error",
                    text: "Check your login credentials and try again."
                })
            }
        })

        // localStorage.setItem('email', email);
        // setUser({email: localStorage.getItem('email')});

        setEmail("");
        setPassword("");
    }

    return (
        (user.id !== null) ?
        <Navigate to='/courses' />
        :
        <Form onSubmit={e => loginUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required 
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="userPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)} 
                    required 
                />
            </Form.Group>
            {   isActive ?
                <Button variant="success" type="submit" id="submitBtn">Login</Button>
                : 
                <Button variant="success" type="submit" id="submitBtn" disabled>Login</Button>
            }  
        </Form>
    );
}