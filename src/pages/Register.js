import { Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from 'react-router-dom';
import { UserContext } from '../UserContext';
import Swal from 'sweetalert2';

export default function Register()
{
    const { user } = useContext(UserContext);
    const [email, setEmail] = useState(""),
        [firstName, setFirstName] = useState(""),
        [lastName, setLastName] = useState(""),
        [mobileNo, setMobileNo] = useState(""),
        [password1, setPassword1] = useState(""),
        [password2, setPassword2] = useState(""),
        [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    useEffect(() => 
    { 
        (firstName !== "" && lastName !== "" && (mobileNo.length === 11) && email !== "" && password1 !== "" && password1 === password2) ? setIsActive(true) : setIsActive(false);
    }, [firstName, lastName, mobileNo, email, password1, password2]);

    function registerUser(e)
    {
        e.preventDefault();
        console.log(email)
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,
        {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => 
        {
            console.log(data)
            if(!data)
            {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`,
                {
                    method: 'POST',
                    headers: {
                        'Content-type':'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => 
                {
                    console.log(data)
                    if (data) 
                    {
                        Swal.fire({
                            title: "Registration successful.",
                            icon: "success",
                            text: "Please login to start."
                        })
                        document.querySelector('#registerForm').reset();
                        navigate('/login');
                    } 
                    else 
                    {
                        Swal.fire({
                            title: "Registration failed.",
                            icon: "error",
                            text: "Something went wrong. Please reload the page."
                        })
                    }
                })
            }
            else
            {
                Swal.fire({
                    title: "Email already registered",
                    icon: "error",
                    text: "Please input another email."
                })
            }
        })
    }

    return (
        (user.id !== null) ?
        <Navigate to='/courses' />
        :
        <Form id="registerForm" onSubmit={e => registerUser(e)}>
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="First Name"
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)} 
                    required 
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Last Name"
                    value={lastName}
                    onChange={e => setLastName(e.target.value)} 
                    required 
                />
            </Form.Group>
            <Form.Group controlId="userMobileNo">
                <Form.Label>Mobile No</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="09123456789"
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)} 
                    required 
                />
            </Form.Group>
            <hr />

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required 
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)} 
                    required 
                />
            </Form.Group>
            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)} 
                    required 
                />
            </Form.Group>
            {   isActive ?
                <Button variant="primary" type="submit" id="submitBtn">Register</Button>
                : 
                <Button variant="primary" type="submit" id="submitBtn" disabled>Register</Button>
            }  
        </Form>
    );
}