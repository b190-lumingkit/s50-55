import React from 'react';

/* 
    createContext() - allows to store different data inside an object, in this case called UserContext. This can be shared to other components within the app
*/
export const UserContext = React.createContext();

/* 
    Provider - allows other components to consume/use the context object and supply the necessary information needed to the context object
*/
// export without default needs destructuring before being able to be imported by other components
export const UserProvider = UserContext.Provider;