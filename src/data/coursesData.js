const coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel risus id nunc rutrum sodales eget nec magna. Proin eleifend bibendum finibus. Interdum et malesuada fames ac ante ipsum primis in faucibus.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python-Django",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel risus id nunc rutrum sodales eget nec magna. Proin eleifend bibendum finibus. Interdum et malesuada fames ac ante ipsum primis in faucibus.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel risus id nunc rutrum sodales eget nec magna. Proin eleifend bibendum finibus. Interdum et malesuada fames ac ante ipsum primis in faucibus.",
        price: 40000,
        onOffer: true
    },
];

export default coursesData;